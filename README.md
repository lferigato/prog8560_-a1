                                                        ------README-------

Since it's a Web Application there's no need to have an installer of any kind. You just have to open the project in Visual Studio 2019, 
using the nugget package console at the bottom of the IDE you have to write the following command: Update-Database.Then you just have 
to run the application by pressing Ctrl+F5. A web browser will open and the application will be available to use.

License:

I chose the MIT License because it's a short and simple permissive license with conditions only requiring preservation of copyright and license notices. 
Licensed works, modifications, and larger works may be distributed under different terms and without source code.