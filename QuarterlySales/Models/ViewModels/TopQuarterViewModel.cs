﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuarterlySales.Models.ViewModels
{
    public class TopQuarterViewModel
    {
        public List<Employee> Employees { get; set; }

        public List<Sales> Sales { get; set; }

        public double LowestAmountToDisplay { get; set; }

        public int NumberOfEmployeesToDisplay { get; set; }
    }
}
