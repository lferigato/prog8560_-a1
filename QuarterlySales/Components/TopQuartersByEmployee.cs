﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuarterlySales.Models;
using QuarterlySales.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuarterlySales.Components
{
    public class TopQuartersByEmployee : ViewComponent
    {
        private SalesContext context;


        public TopQuartersByEmployee(SalesContext saleContext)
        {
            context = saleContext;
        }

        public IViewComponentResult Invoke(double lowestAmountToDisplay, int numberOfEmployeesToDisplay)
        {
            var sales = context.Sales
                .Include(e => e.Employee)
                .Where(w => w.Amount >= lowestAmountToDisplay)
                .OrderBy(ob => ob.Amount.HasValue)
                .ToList();

            var viewModel = new TopQuarterViewModel()
            {
                Sales = sales,
                LowestAmountToDisplay = lowestAmountToDisplay,
                NumberOfEmployeesToDisplay = Math.Min(sales.Count, numberOfEmployeesToDisplay)
            };

            return View(viewModel);
        }
    }
}
